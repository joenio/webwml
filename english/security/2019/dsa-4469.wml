<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in Libvirt, a virtualisation
abstraction library, allowing an API client with read-only permissions
to execute arbitrary commands via the virConnectGetDomainCapabilities
API, or read or execute arbitrary files via the
virDomainSaveImageGetXMLDesc API.</p>

<p>Additionally the libvirt's cpu map was updated to make addressing
<a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">\
CVE-2018-3639</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-5753">\
CVE-2017-5753</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">\
CVE-2017-5715</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12126">\
CVE-2018-12126</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12127">\
CVE-2018-12127</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-12130">\
CVE-2018-12130</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2019-11091">\
CVE-2019-11091</a> easier by supporting the md-clear, ssbd, spec-ctrl
and ibpb CPU features when picking CPU models without having to fall
back to host-passthrough.</p>

<p>For the stable distribution (stretch), these problems have been fixed in
version 3.0.0-4+deb9u4.</p>

<p>We recommend that you upgrade your libvirt packages.</p>

<p>For the detailed security status of libvirt please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/libvirt">\
https://security-tracker.debian.org/tracker/libvirt</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4469.data"
# $Id: $
