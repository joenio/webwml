<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were found in Jetty, a Java based web server
and servlet engine.</p>

<p>The org.eclipse.jetty.servlets.CGI class has been deprecated. It is potentially
unsafe to use it. The upstream developers of Jetty recommend to use Fast CGI
instead. See also <a href="https://security-tracker.debian.org/tracker/CVE-2023-36479">CVE-2023-36479</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26048">CVE-2023-26048</a>

    <p>In affected versions servlets with multipart support (e.g. annotated with
    `@MultipartConfig`) that call `HttpServletRequest.getParameter()` or
    `HttpServletRequest.getParts()` may cause `OutOfMemoryError` when the
    client sends a multipart request with a part that has a name but no
    filename and very large content. This happens even with the default
    settings of `fileSizeThreshold=0` which should stream the whole part
    content to disk.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-26049">CVE-2023-26049</a>

    <p>Nonstandard cookie parsing in Jetty may allow an attacker to smuggle
    cookies within other cookies, or otherwise perform unintended behavior by
    tampering with the cookie parsing mechanism.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40167">CVE-2023-40167</a>

    <p>Prior to this version Jetty accepted the `+` character proceeding the
    content-length value in a HTTP/1 header field. This is more permissive than
    allowed by the RFC and other servers routinely reject such requests with
    400 responses. There is no known exploit scenario, but it is conceivable
    that request smuggling could result if jetty is used in combination with a
    server that does not close the connection after sending such a 400
    response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-36479">CVE-2023-36479</a>

    <p>Users of the CgiServlet with a very specific command structure may have the
    wrong command executed. If a user sends a request to a
    org.eclipse.jetty.servlets.CGI Servlet for a binary with a space in its
    name, the servlet will escape the command by wrapping it in quotation
    marks. This wrapped command, plus an optional command prefix, will then be
    executed through a call to Runtime.exec. If the original binary name
    provided by the user contains a quotation mark followed by a space, the
    resulting command line will contain multiple tokens instead of one.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-41900">CVE-2023-41900</a>

    <p>Jetty is vulnerable to weak authentication. If a Jetty
    `OpenIdAuthenticator` uses the optional nested `LoginService`, and that
    `LoginService` decides to revoke an already authenticated user, then the
    current request will still treat the user as authenticated. The
    authentication is then cleared from the session and subsequent requests
    will not be treated as authenticated. So a request on a previously
    authenticated session could be allowed to bypass authentication after it
    had been rejected by the `LoginService`. This impacts usages of the
    jetty-openid which have configured a nested `LoginService` and where that
    `LoginService` is capable of rejecting previously authenticated users.</p></li>

</ul>

<p>For the oldstable distribution (bullseye), these problems have been fixed
in version 9.4.39-3+deb11u2.</p>

<p>For the stable distribution (bookworm), these problems have been fixed in
version 9.4.50-4+deb12u1.</p>

<p>We recommend that you upgrade your jetty9 packages.</p>

<p>For the detailed security status of jetty9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jetty9">\
https://security-tracker.debian.org/tracker/jetty9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5507.data"
# $Id: $
