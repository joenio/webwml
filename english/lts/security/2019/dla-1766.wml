<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Hanno Böck discovered that GNOME Evolution is prone to OpenPGP signatures
being spoofed for arbitrary messages using a specially crafted HTML email.
This issue was mitigated by moving the security bar with encryption and
signature information above the message headers.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.12.9~git20141130.241663-1+deb8u1.</p>

<p>We recommend that you upgrade your evolution packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1766.data"
# $Id: $
