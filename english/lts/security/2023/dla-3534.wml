<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The RAR archiver allows directory traversal to write to files during an
extract (aka unpack) operation, as demonstrated by creating a
~/.ssh/authorized_keys file.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2:6.20-0.1~deb10u1.</p>

<p>We recommend that you upgrade your rar packages.</p>

<p>For the detailed security status of rar please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/rar">https://security-tracker.debian.org/tracker/rar</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3534.data"
# $Id: $
