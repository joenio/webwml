<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Han Zheng discovered an out-of-bounds write in w3m, a text based web
browser and pager. It can be triggered by sending a crafted HTML file
to the w3m binary. It allows an attacker to cause Denial of Service
(DoS) or possibly have unspecified other impact.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.5.3-37+deb10u1.</p>

<p>We recommend that you upgrade your w3m packages.</p>

<p>For the detailed security status of w3m please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/w3m">https://security-tracker.debian.org/tracker/w3m</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3541.data"
# $Id: $
