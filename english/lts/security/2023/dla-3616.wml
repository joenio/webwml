<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential code injection vulnerability in
<code>org-mode</code>, a popular add-on for the Emacs text editor.</p>

<p>Attackers could have executed arbitrary shell commands via a filename (or
directory name) that contained shell metacharacters.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28617">CVE-2023-28617</a>

    <p>org-babel-execute:latex in ob-latex.el in Org Mode through 9.6.1 for GNU
    Emacs allows attackers to execute arbitrary commands via a file name or
    directory name that contains shell metacharacters.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
9.1.14+dfsg-3+deb10u1.</p>

<p>We recommend that you upgrade your org-mode packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3616.data"
# $Id: $
