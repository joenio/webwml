<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in libXpm, the X Pixmap (XPM) image
library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43786">CVE-2023-43786</a>

    <p>Yair Mizrahi discovered an infinite recursion issue when parsing
    crafted XPM files, which would result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43787">CVE-2023-43787</a>

    <p>Yair Mizrahi discovered a buffer overflow vulnerability in libX11
    when parsing crafted XPM files, which could result in denial of
    service or potentially the execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43788">CVE-2023-43788</a>

    <p>Alan Coopersmith found an out of bounds read in
    XpmCreateXpmImageFromBuffer, which could result in denial of
    service when parsing crafted XPM files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-43789">CVE-2023-43789</a>

    <p>Alan Coopersmith discovered an out of bounds read issue when
    parsing corrupted colormaps, which could lead to denial of
    service when parsing crafted XPM files.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:3.5.12-1+deb10u2.</p>

<p>We recommend that you upgrade your libxpm packages.</p>

<p>For the detailed security status of libxpm please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxpm">https://security-tracker.debian.org/tracker/libxpm</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3603.data"
# $Id: $
