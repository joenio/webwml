<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>node-nth-check, is a NodeJS module module used to parse and compile
nth-checks, as they are found in CSS 3's nth-child() and
nth-last-of-type() functions.</p>

<p>This module was vulnerable to a regular expression denial of service
used for parsing.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.0.1-1+deb10u1.</p>

<p>We recommend that you upgrade your node-nth-check packages.</p>

<p>For the detailed security status of node-nth-check please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-nth-check">https://security-tracker.debian.org/tracker/node-nth-check</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3428.data"
# $Id: $
