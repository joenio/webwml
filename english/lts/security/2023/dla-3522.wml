<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were discovered in HDF5, a Hierarchical
Data Format and a library for scientific data. Memory leaks, out-of-bound
reads and division by zero errors may lead to a denial of service when
processing a malformed HDF file.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.10.4+repack-10+deb10u1.</p>

<p>We recommend that you upgrade your hdf5 packages.</p>

<p>For the detailed security status of hdf5 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/hdf5">https://security-tracker.debian.org/tracker/hdf5</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3522.data"
# $Id: $
