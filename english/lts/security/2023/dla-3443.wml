<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in the network traffic analyzer Wireshark.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2856">CVE-2023-2856</a>

    <p>VMS TCPIPtrace file parser crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2858">CVE-2023-2858</a>

    <p>NetScaler file parser crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2879">CVE-2023-2879</a>

    <p>GDSDB infinite loop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2952">CVE-2023-2952</a>

    <p>XRA dissector infinite loop</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.6.20-0+deb10u7.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>For the detailed security status of wireshark please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wireshark">https://security-tracker.debian.org/tracker/wireshark</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3443.data"
# $Id: $
