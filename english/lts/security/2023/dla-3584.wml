<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Florent Saudel and Arnaud Gatignol discovered a Type Confusion
vulnerability in the Spotlight RPC functions in afpd in Netatalk. When
parsing Spotlight RPC packets, one encoded data structure is a key-value
style dictionary where the keys are character strings, and the values can
be any of the supported types in the underlying protocol. Due to a lack of
type checking in callers of the dalloc_value_for_key() function, which
returns the object associated with a key, a malicious actor may be able to
fully control the value of the pointer and theoretically achieve Remote
Code Execution on the host.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.1.12~ds-3+deb10u4.</p>

<p>We recommend that you upgrade your netatalk packages.</p>

<p>For the detailed security status of netatalk please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/netatalk">https://security-tracker.debian.org/tracker/netatalk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3584.data"
# $Id: $
