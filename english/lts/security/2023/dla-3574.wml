<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two NULL pointer dereference flaws were discovered in Mutt, a text-based
mailreader supporting MIME, GPG, PGP and threading, which may result in denial
of service (application crash) when viewing a specially crafted email or when
composing from a specially crafted draft message.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.10.1-2.1+deb10u7.</p>

<p>We recommend that you upgrade your mutt packages.</p>

<p>For the detailed security status of mutt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mutt">https://security-tracker.debian.org/tracker/mutt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3574.data"
# $Id: $
