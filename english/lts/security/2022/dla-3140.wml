<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential SQL injection vulnerability
in libpgjava, a Java library for connecting to PostgreSQL databases.</p>

<p>A malicious user could have crafted a schema that caused an application to
execute commands as a privileged user due to the lack of escaping of column
names in some operations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31197">CVE-2022-31197</a>

    <p>PostgreSQL JDBC Driver (PgJDBC for short) allows Java programs to
    connect to a PostgreSQL database using standard, database independent Java
    code. The PGJDBC implementation of the `java.sql.ResultRow.refreshRow()`
    method is not performing escaping of column names so a malicious column
    name that contains a statement terminator, e.g. `;`, could lead to SQL
    injection. This could lead to executing additional SQL commands as the
    application's JDBC user. User applications that do not invoke the
    `ResultSet.refreshRow()` method are not impacted. User application that do
    invoke that method are impacted if the underlying database that they are
    querying via their JDBC application may be under the control of an
    attacker. The attack requires the attacker to trick the user into executing
    SQL against a table name who's column names would contain the malicious SQL
    and subsequently invoke the `refreshRow()` method on the ResultSet. Note
    that the application's JDBC user and the schema owner need not be the same.
    A JDBC application that executes as a privileged user querying database
    schemas owned by potentially malicious less-privileged users would be
    vulnerable. In that situation it may be possible for the malicious user to
    craft a schema that causes the application to execute commands as the
    privileged user. Patched versions will be released as `42.2.26` and
    `42.4.1`. Users are advised to upgrade. There are no known workarounds for
    this issue.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
42.2.5-2+deb10u2.</p>

<p>We recommend that you upgrade your libpgjava packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3140.data"
# $Id: $
