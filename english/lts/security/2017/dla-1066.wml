<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A stack-based buffer overflow in the zend_ini_do_op() function in
Zend/zend_ini_parser.c could cause a denial of service or potentially allow
executing code. NOTE: this is only relevant for PHP applications that accept
untrusted input (instead of the system's php.ini file) for the parse_ini_string
or parse_ini_file function, e.g., a web application for syntax validation of
php.ini directives.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.4.45-0+deb7u10.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1066.data"
# $Id: $
