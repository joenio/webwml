<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in exiv2, a C++ library and
a command line utility to manage image metadata, resulting in denial of
service, heap-based buffer over-read/overflow, memory exhaustion, and
application crash.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.24-4.1+deb8u1.</p>

<p>We recommend that you upgrade your exiv2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1402.data"
# $Id: $
