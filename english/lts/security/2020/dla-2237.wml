<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following CVE(s) were reported against src:cups.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8842">CVE-2019-8842</a>

    <p>The `ippReadIO` function may under-read an extension field.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3898">CVE-2020-3898</a>

    <p>There was a heap based buffer overflow in libcups's
    ppdFindOption() in ppd-mark.c.
    The `ppdOpen` function did not handle invalid UI constraint.
    `ppdcSource::get_resolution` function did not handle invalid
    resolution strings.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.5-11+deb8u8.</p>

<p>We recommend that you upgrade your cups packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2237.data"
# $Id: $
