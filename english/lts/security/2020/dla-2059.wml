<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in git, a fast, scalable,
distributed revision control system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1348">CVE-2019-1348</a>

    <p>It was reported that the --export-marks option of git fast-import is
    exposed also via the in-stream command feature export-marks=...,
    allowing to overwrite arbitrary paths.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1387">CVE-2019-1387</a>

    <p>It was discovered that submodule names are not validated strictly
    enough, allowing very targeted attacks via remote code execution
    when performing recursive clones.</p>

<p>In addition this update addresses a number of security issues which are
only an issue if git is operating on an NTFS filesystem (<a href="https://security-tracker.debian.org/tracker/CVE-2019-1349">CVE-2019-1349</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.1.4-2.1+deb8u8.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2059.data"
# $Id: $
