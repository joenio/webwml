#use wml::debian::template title="Cryptografische software in het hoofdarchief van Debian - een verkenning" BARETITLE=true
#use wml::debian::translation-check translation="ba01cfdc529712e3626bdf15fd37d39e94126794"

# Nota bene! This is basically draft text from the lawyers, and it must
# _not_ be modified for spelling errors and such. Formatting changes are
# fine (I think). -- Joy

<hr />

<p>
Noot van de vertaler:&nbsp; deze vertaling is slechts ter informatie.
De originele tekst is te vinden in de
<a href="$(HOME)/legal/cryptoinmain.en.html">Engelse versie</a>.
</p>

<hr />

<table width="100%" summary="mail headers">
<colgroup span="2">
<col width="10%">
<col>
</colgroup>
<tr><td>Aan:</td>
	<td><a href="https://www.spi-inc.org/">Software in the Public Interest</a>, <a href="https://www.debian.org/">Debian Project</a></td></tr>
<tr><td>Van:</td>
	<td>Roszel C. Thomsen II, Partner, <a href="http://www.t-b.com/">Thomsen &amp; Burke LLP</a></td></tr>
<tr><td>Datum:</td>
	<td>31 juli 2001</td></tr>
<tr><td>Betreft:</td>
	<td>Cryptografische software in het hoofdarchief van Debian - een verkenning</td></tr>
</table>

<p>Dank u voor deze gelegenheid om te reageren op Sam Hartman's technisch rapport getiteld <q>Cryptografische software in het hoofdarchief van Debian - een verkenning</q>.</p>

<p>Wij verstrekken u deze informatie als een algemene richtlijn. BXA eist dat elke entiteit die producten uitvoert, bekend is met en voldoet aan haar positieve verplichtingen die zijn vastgelegd in de voorschriften van de exportadministratie. Houd er rekening mee dat de voorschriften aan verandering onderhevig zijn. Wij raden u aan uw eigen juridisch advies in te winnen wanneer u probeert te exporteren. Bovendien kunnen sommige landen bepaalde niveaus van encryptie beperken bij invoer in hun land. Wij raden u aan juridische adviseurs in het betreffende land of de toepasselijke overheidsinstanties in het betreffende land te raadplegen.</p>

<p>Ter informatie: de uitvoer van cryptografische software uit de Verenigde Staten is onderworpen aan de voorschriften van de Amerikaanse exportadministratie (Export Administration Regulations - <q>EAR</q>, 15 CFR deel 730 en volgende), die worden beheerd door het bureau voor exportadministratie van het ministerie van Handel (Bureau of Export Administration - <q>BXA</q>). BXA heeft de bepalingen van de EAR met betrekking tot cryptografische software voor het laatst herzien op 19 oktober 2000. Ik noem deze de <q>nieuwe VS-voorschriften</q> om ze te onderscheiden van eerdere voorschriften die restrictiever waren.</p>

<p>Toen de regering-Clinton naar Washington kwam, werd de uitvoer uit de Verenigde Staten van encryptieproducten gecontroleerd als  was het <q>munitie</q> krachtens de wet op de controle op wapenexport en de verordeningen inzake de internationale wapenhandel. De meeste aanvragen voor vergunningen voor de uitvoer van sterke encryptieproducten werden afgewezen. De industrie en belangengroepen lobbyden voor liberalisering, en de regering-Clinton hervormde de verouderde Amerikaanse exportcontrole op encryptie-artikelen in een reeks graduele stappen, met als hoogtepunt de nieuwe Amerikaanse regelgeving. De nieuwe regering-Bush overweegt verdere liberaliseringen die later dit jaar gepubliceerd kunnen worden.</p>

<p>Ondanks deze liberalisering blijven de Amerikaanse uitvoercontroles op commerciële encryptieproducten complex en zwaar.  Amerikaanse bedrijven moeten encryptieproducten vóór uitvoer aan een technische controle door de inlichtingendiensten onderwerpen. Voor de uitvoer naar sommige instanties van buitenlandse regeringen zijn vergunningen vereist, evenals voor de uitvoer naar aanbieders van telecommunicatie- en internetdiensten die diensten willen verlenen aan sommige overheidsinstanties. Tenslotte gelden voor veel uitvoer uit de Verenigde Staten rapportageverplichtingen na de uitvoer.  De Amerikaanse controles op de uitvoer van encryptie blijven dus een aanzienlijke regelgevende last leggen op Amerikaanse bedrijven en vertragen de wereldwijde invoering van sterke cryptografie in commerciële softwareprogramma's.</p>

<p>Niet alle softwareprogramma's met encryptie zijn echter commerciële producten.  In het kader van de EAR kan de controle op cryptografische broncode in drie categorieën worden ingedeeld: a) open broncode, b) open broncode van en voor de gemeenschap c) gepatenteerde broncode.  De regels voor de uitvoer van elk type broncode zijn verschillend, en zij zijn op belangrijke punten gewijzigd in de nieuwe VS-verordeningen.</p>

<p>Open broncode verwijst naar software die vrij en zonder beperkingen beschikbaar is voor het publiek, onder een GNU-achtige licentieovereenkomst.  Debian lijkt in deze categorie te vallen. De oude regelgeving stond de export van open broncode naar elke eindgebruiker toe zonder technische beoordeling, op voorwaarde dat de persoon die de open broncode beschikbaar stelde een gelijktijdige melding indiende bij BXA en het nationaal veiligheidsagentschap (National Security Agency - <q>NSA</q>). De oude regelgeving zweeg echter over eventuele beperkingen op de uitvoer van gecompileerde uitvoerbare software die is afgeleid van openbronsoftware.</p>

<p>Onder de nieuwe Amerikaanse regelgeving komt niet alleen de open broncode, maar ook de gecompileerde uitvoerbare software die is afgeleid van open broncode in aanmerking voor export onder dezelfde voorwaarden als de open broncode zelf, op voorwaarde dat de gecompileerde uitvoerbare software onbeperkt en vrij beschikbaar is. Als u de gecompileerde uitvoerbare software opneemt in een product dat u tegen betaling distribueert, is het resulterende product helaas onderworpen aan alle regels die van toepassing zijn op commerciële softwareprogramma's. Ze moeten bijvoorbeeld worden ingediend bij BXA en NSA voor een eenmalige technische beoordeling, zoals hierboven beschreven.</p>

<p>Open broncode van en voor de gemeenschap verwijst naar software die vrij beschikbaar is voor het publiek voor niet-commercieel gebruik, maar die verdere beperkingen bevat voor commercieel gebruik. Open broncode van en voor de gemeenschap kan worden geëxporteerd onder in wezen dezelfde voorwaarden als open broncode, maar open broncode van en voor de gemeenschap blijft onderworpen aan meer gedetailleerde rapportagevereisten.</p>

<p>Gepatenteerde broncode verwijst naar alle broncode die noch <q>open</q> noch <q>van en voor de gemeenschap</q> is. Exporteurs mogen gepatenteerde broncode verstrekken aan elke eindgebruiker in de EU en haar partners, en aan elke niet-gouvernementele eindgebruiker in andere landen, onmiddellijk na het indienen van een technische beoordeling bij BXA en NSA. Dezelfde rapportagevereisten die van toepassing zijn op open broncode van en voor de gemeenschap, zijn ook van toepassing op gepatenteerde broncode.</p>

<p>Houd er rekening mee dat personen in de VS die dingen plaatsen op sites buiten de VS, onder de Amerikaanse wetgeving vallen, zelfs als ze dit op persoonlijke titel doen. Daarom wilt u misschien personen in de VS waarschuwen dat wat ze plaatsen op de huidige cryptoserver buiten de VS nog steeds onderworpen zijn aan Amerikaanse regelgeving.</p>

<p>Ten slotte dient u zich ervan bewust te zijn dat op alle uitvoer van openbron cryptografische software uit de Verenigde Staten een kernpakket van Amerikaanse exportcontroles van toepassing is. In wezen verbieden deze controles de uitvoer van openbron cryptografische software onder licentie-uitzondering TSU naar (1) verboden partijen (vermeld op <a href="http://www.bxa.doc.gov/DPL/Default.shtm"> http://www.bxa.doc.gov/DPL/Default.shtm</a>), (2) verboden landen (momenteel Cuba, Iran, Irak, Libië, Noord-Korea, Soedan, Syrië en het door de Taliban bezette gedeelte van Afghanistan) en (3) het ontwerpen, ontwikkelen, opslaan, produceren of gebruiken van nucleaire, chemische of biologische wapens of raketten.</p>

<p>Met deze achtergrond worden uw specifieke vragen met betrekking tot Debian behandeld in de volgorde waarin ze voorkomen in het technisch rapport van Sam Hartman, hieronder cursief weergegeven.</p>

<hr />

<h2><i>Cryptografische software in het hoofdarchief van Debian - een verkenning</i></h2>

<p><i>Sam Hartman</i></p>

<p><i>Debian Project</i></p>

<hrline />

<p style="margin-left: 2em">
    <i>Debian is een vrij besturingssysteem. Momenteel splitst Debian om Amerikaanse exportredenen de cryptografische software af in een apart archief buiten de VS. Dit document vat de vragen samen die we vanuit juridisch oogpunt zouden moeten beantwoorden om deze twee archieven samen te voegen.</i>
</p>

<hrline />

<h3><i>Over Debian</i></h3>

<p><i>Debian is een groep individuen die werken aan de productie van een vrij besturingssysteem. Deze individuen zijn verantwoordelijk voor beslissingen die ze maken terwijl ze aan Debian werken; er is geen wettelijke Debian-organisatie waarvoor ontwikkelaars werken of waarvoor beslissingen worden genomen. Er bestaat een geregistreerde non-profitorganisatie, Software in het algemeen belang (Software in the Public Interest - SPI), die geld en middelen voor Debian beheert. Dus beslissingen van ontwikkelaars kunnen gevolgen hebben voor middelen die eigendom zijn van SPI en dus gevolgen hebben voor SPI. Andere Debian-middelen zijn eigendom van verschillende sponsors. Debian is over het algemeen afhankelijk van sponsors voor netwerkverbindingen. Er zijn ook groepen van derden die de Debian-software naar spiegelservers kopiëren, zodat mensen over de hele wereld deze kunnen downloaden en gebruiken. Anderen maken en verkopen cd's van Debian. Al deze groepen kunnen in meer of mindere mate verantwoordelijk zijn voor de beslissingen die Debian neemt. We willen ons gedragen op een manier die de aansprakelijkheid voor alle partijen minimaliseert en, binnen die beperking, de waarde van onze inspanningen maximaliseert.</i></p>

<p><i>Zoals dit het geval is voor alle leveranciers van besturingssystemen, moet Debian cryptografische software bevatten. Deze software zorgt voor veiligheid, laat gebruikers toe deel te nemen aan internethandel en vervult andere taken die cryptografie vereisen. Momenteel wordt deze software opgeslagen op een server buiten de Verenigde Staten, de zogenaamde <q>non-US server</q>. Momenteel neemt Debian geen maatregelen om Amerikaanse ontwikkelaars te helpen bij het volgen van exportcontrolevoorschriften als ze software uploaden naar het archief non-US of om te voorkomen dat ze software uploaden. We willen graag cryptografische software van de non-US server verplaatsen naar onze hoofdserver in de VS.</i></p>

<p><i>Met het toenemende netwerkkarakter van het werk en het feit dat steeds meer kritische functies op computerplatforms worden geplaatst, en de betreurenswaardige toename van onheil en opzettelijke kwaadwilligheid, zal beveiliging steeds belangrijker worden. Cryptografie is een belangrijke hoeksteen van een aantal beveiligingsprocessen. Elk besturingssysteem dat zich niet inspant om cryptografie naadloos te integreren, zal waarschijnlijk niet concurrentieel zijn.</i></p>

<p><i>Door alle software op één enkele bron te plaatsen en de bijbehorende mogelijkheid om een enkele set cd's te maken met geïntegreerde cryptografische ondersteuning, wordt het gemakkelijker voor de gebruikers, wordt het gemakkelijker voor cd-leveranciers, wordt het voor ontwikkelaars eenvoudiger om software naar deze sites te uploaden, en vereenvoudigt de taak van het repliceren van de softwareopslagplaatsen op het internet.</i></p>

<p><i>De rest van dit document richt zich op de hoofdserver in de VS en op zijn spiegelservers en kopieën over de hele wereld. Het is belangrijk te beseffen dat er momenteel een parallelle structuur is opgezet voor de non-US server.</i></p>

<p><i>Om de paar maanden brengen Debian-ontwikkelaars een nieuwe officiële versie van Debian uit. Deze software wordt op de hoofdserver (en voor cryptografische software de non-US server) beschikbaar gesteld aan een groep primaire spiegelservers over de hele wereld. Deze spiegelservers kopiëren de software van de hoofdserver en maken deze beschikbaar voor gebruikers en secundaire spiegelservers. De gebruikers kunnen HTTP, FTP of diverse andere methoden gebruiken om de software op te halen. Cd-images zijn beschikbaar voor gebruikers en wederverkopers. Deze images kunnen op fysieke cd's worden gebrand door individuen of door degenen die Debian willen verkopen/weggeven.</i></p>

<p><i>Daarnaast zijn er twee releases van Debian die voortdurend evolueren: de releases testing en unstable. Deze releases worden dagelijks bijgewerkt door ontwikkelaars over de hele wereld. Net als de officiële releases, worden deze releases beschikbaar gesteld op de hoofdserver en non-US server voor primaire spiegelservers. De primaire spiegelservers stellen software beschikbaar via HTTP, FTP en andere methoden, zowel voor eindgebruikers als voor secundaire spiegelservers. Van deze releases worden soms cd-images gemaakt. Het belangrijke verschil tussen deze evoluerende releases en de officiële release is dat ze voortdurend veranderen.</i></p>

<p><i>Ontwikkelaars uploaden vaak binaire bestanden en broncode tegelijkertijd. We ondersteunen echter veel verschillende soorten computers, die allemaal verschillende binaire bestanden nodig hebben voor dezelfde broncode. De meeste ontwikkelaars maken alleen binaire bestanden voor een van de computerarchitecturen die we ondersteunen wanneer ze een aangepast programma uploaden. Geautomatiseerde processen gebruiken de broncode die ze hebben geüpload om binaire bestanden te maken voor de andere architecturen. Als zodanig zullen sommige binaire bestanden voor een bepaald broncodeprogramma waarschijnlijk op een later tijdstip dan die broncode worden geüpload.</i></p>

<p><i>Sommige Debian-ontwikkelaars gebruiken ook middelen van Debian om te werken aan nog niet uitgebrachte software. De primaire bron die hiervoor nuttig is, is de Debian CVS-server. De broncode van projecten op deze server is bijna altijd beschikbaar voor het publiek, maar kan vele malen per dag veranderen. De CVS-server staat in de VS.</i></p>

<p><i>De meeste Debian-software wordt echter niet rechtstreeks door Debian-ontwikkelaars ontwikkeld. In plaats daarvan wordt de software openbaar gemaakt door een derde partij. Sommige software wordt beschikbaar gesteld aan het publiek op sites binnen de VS, terwijl andere oorspronkelijke auteurs hun software uitbrengen op sites buiten de VS. Debian-ontwikkelaars zijn verantwoordelijk voor de integratie van de software in Debian. Als onderdeel van deze taak werken veel Debian-ontwikkelaars nauw samen met de oorspronkelijke software-auteur en dragen vaak code bij aan de oorspronkelijke release.</i></p>

<p><i>De software in Debian voldoet aan de Debian Richtlijnen voor Vrije Software; de DFSG (Debian Free Software Guidelines). Wij geloven dat deze software openbaar beschikbare broncode heeft in de zin van sectie 740.13(e) van de EAR. De richtlijnen vereisen dat de broncode herverdeelbaar is. De DFSG vereist indirect dat men een product gebaseerd op de broncode kan verspreiden zonder een vergoeding te betalen. Wij verspreiden alle broncode in Debian als onderdeel van onze releases. Andere (niet-vrije) software wordt verspreid in ons archief non-free, maar in dit document richten wij ons op de DFSG-vrije software. We zouden graag willen weten in hoeverre we niet-DFSG-vrije software waarvoor we broncode kunnen distribueren, naar de VS kunnen verplaatsen, maar we willen ervoor zorgen dat advies op dit gebied niet wordt verward met advies over hoe om te gaan met DFSG-vrije software.</i></p>

<p><i>Debian-ontwikkelaars wonen over de hele wereld en zijn burgers van vele landen. Uiteraard zijn sommigen Amerikaans staatsburger, maar vele anderen niet. Sommigen kunnen burgers zijn van de zeven verboden landen uit de EAR sectie 740.13(e).</i></p>

<p><i>Zoals gezegd hebben we spiegelservers over de hele wereld. Wij hebben geen officiële spiegelservers (spiegelservers waarmee het project is verbonden) in een van de zeven landen die in EAR sectie 740.13(e) worden genoemd, hoewel onze software, aangezien deze publiekelijk beschikbaar is, mogelijk naar deze landen wordt gekopieerd. De meeste spiegelservers in de VS spiegelen momenteel alleen de hoofdserver (degene zonder cryptografie), hoewel sommige spiegelservers zowel het hoofdgedeelte als het non-US gedeelte van het archief spiegelen. Debian neemt geen verantwoordelijkheid voor spiegelservers in de VS die het non-US deel van het archief spiegelen.</i></p>

<hrline />

<h3><i>Onze doelstelling</i></h3>

<p><i>We willen cryptografische software opnemen in ons hoofdarchief. We willen de risico's begrijpen voor ontwikkelaars, gebruikers, SPI, spiegelserverbeheerders, cd-verkopers, sponsors en alle andere partijen die verbonden zijn met Debian, zodat we een weloverwogen beslissing kunnen nemen. Wij willen deze risico's kunnen documenteren en naar buiten brengen, zodat deze partijen niet uit onwetendheid een misdaad begaan. Uiteraard willen we ook geen onnodige risico's nemen.</i></p>

<p><i>In het bijzonder willen we de volgende activiteiten overwegen:</i></p>

<ul>
<li><i>Dagelijks DFSG-vrije software toevoegen aan en aanpassen aan onze releases. In de praktijk worden alleen de releases testing en unstable dagelijks gewijzigd, maar ook de andere releases worden van tijd tot tijd aangepast.</i></li>

<li><i>Cryptografische software verspreiden als onderdeel van onze releases via internet en op cd's.</i></li>

<li><i>DFSG-vrije cryptografische software toevoegen en wijzigen op onze CVS-server.</i></li>

<li><i>Alle reacties die we zouden moeten hebben op eventuele wijzigingen in cryptografische voorschriften (of wetten).</i></li>
</ul>

<hrline />

<p><em>EINDE van de Preambule en van het Document van Debian</em></p>

<p>Ik zal proberen deze doelstellingen weer te geven in mijn antwoorden op uw vragen.  Samengevat denk ik dat een eerste kennisgeving moet volstaan voor het huidige archief en de updates daarvan.  Een nieuwe kennisgeving zou alleen nodig zijn als een nieuw programma met encryptie aan het archief wordt toegevoegd. Voor de verdere verspreiding van freeware is geen verdere kennisgeving vereist.  Voor commerciële versies gelden echter de technische evaluatie-, licentie- en rapportagevoorschriften die van toepassing zijn op andere commerciële producten. Het is moeilijk te voorspellen of de wet- en regelgeving in de toekomst zal veranderen, maar als de wet verandert, zult u de site moeten verwijderen of aanpassen om aan de voorschriften te blijven voldoen. U bent niet verplicht andere achterbannen te informeren over hun wettelijke verplichtingen, maar als u een lijst van veelgestelde vragen hebt, stel ik u graag passende antwoorden voor.</p>

<p>Vragen (Let op, elke vraag van Debian is gemarkeerd met "D:")</p>

<blockquote class="question">
<p>
<span>D:</span>
	Moeten wij het bureau voor exportadministratie (BXA) in kennis stellen van software die wij aan releases toevoegen?
</p>
</blockquote>

<p>Als de kennisgeving correct is opgesteld en het archief op de in de kennisgeving vermelde site blijft, hoeft u slechts één kennisgeving bij BXA in te dienen voor het oorspronkelijke archief.  Er is slechts één kennisgeving voor één locatie in de VS vereist; er is geen afzonderlijke kennisgeving vereist voor spiegelserversites binnen of buiten de VS. Deze kennisgeving zou alleen moeten worden bijgewerkt als u een nieuw programma toevoegt dat encryptie toepast.</p>

<pre>
	Ministerie van Handel
	Bureau voor exportadministratie
	Bureau voor de strategische controle van handel en buitenlands beleid
	14de Straat en Pennsylvania Ave., N.W.
	Kamer 2705
	Washington, DC 20230

	Betreft:  Kennisgeving van versleutelingsbroncode zonder restricties
	Product: Debian Broncode

	Geachte heer/mevrouw,
	Op grond van paragraaf (e)(1) van deel 740.13 van de reglementering van de Amerikaanse exportadministratie, de U.S. Export Administration Regulations ("EAR", 15 CFR deel 730 e.v.), verstrekken wij deze schriftelijke kennisgeving van de internetlocatie van de zonder beperkingen openbaar beschikbare broncode van Debian. De broncode van Debian is een vrij besturingssysteem dat ontwikkeld wordt door een groep individuen en gecoördineerd wordt door de non-profit organisatie Software in the Public Interest (Software in het algemeen belang). Dit archief wordt van tijd tot tijd bijgewerkt, maar de locatie is constant. Daarom dient deze kennisgeving als een eenmalige kennisgeving voor latere updates die in de toekomst kunnen plaatsvinden. Voor nieuwe programma's zal een afzonderlijke kennisgeving worden gedaan. De internetlocatie voor de Debian-broncode is: http://ftp.debian.org/debian/

	  Deze site wordt gespiegeld door een aantal andere sites buiten de Verenigde Staten.

	  Een duplicaat van deze kennisgeving is toegezonden aan de ENC-coördinator voor encryptieaanvragen (Encryption Request Coordinator), P.O. Box 246, Annapolis Junction, MD 20701-0246.

	  Als u vragen hebt, bel me dan op (xxx) xxx-xxxx.

	Hoogachtend,
	  Naam
	  Titel
</pre>


<blockquote class="question">
<p>
<span>D:</span>
	Welke informatie moeten wij in de kennisgevingen opnemen?
</p>
</blockquote>

<p>Het ontwerp hierboven bevat de informatie die u in de kennisgeving moet opnemen</p>


<blockquote class="question">
<p>
<span>D:</span>
	Hoe vaak moeten we melden? We willen zo min mogelijk melden omdat het meer werk voor ons en voor de overheid oplevert, maar we willen zo vaak melden als nodig is om aan de regelgeving te voldoen.
</p>
</blockquote>

<p>Zoals hierboven uiteengezet, en in de veronderstelling dat het archief op de in de kennisgeving vermelde internetsite blijft, zou u geen latere kennisgeving moeten indienen voor latere updates.  U zou alleen een nieuwe kennisgeving moeten indienen als u een nieuw programma toevoegt dat cryptografie implementeert.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Als we onze cryptografische software naar dit land verplaatsen, en de wetten of regels veranderen en worden restrictiever, wat zullen we dan waarschijnlijk verliezen?  Zouden we software of cd's moeten vernietigen?  Zouden we de software moeten verwijderen van onze hoofdsite of van secundaire sites?  Als we de toegenomen beschikbaarheid van cryptografische software gebruiken om de veiligheid van de rest van het systeem te verbeteren, en het juridische klimaat voor cryptografie verslechtert, is het dan waarschijnlijk dat we alle kopieën van dergelijke software in de VS zouden moeten weggooien?
</p>
</blockquote>

<p>De tendens is eerder naar een grotere liberalisering van de uitvoercontroles op cryptografie in de Verenigde Staten dan naar meer beperkingen.  Deze trend is de afgelopen tien jaar constant geweest en is het afgelopen jaar versneld.  Wij kunnen u geen advies geven over wat u zou kunnen verliezen, tenzij pas wanneer nieuwe voorschriften worden gepubliceerd. Wij denken echter dat u de auteursrechten op de software en enkele, zij het misschien beperktere, exportrechten zou behouden.</p>


<blockquote class="question">
<p>
<span>D:</span>
	In volgorde van afnemende voorkeur, willen we graag een kennisgeving doen:
        </p>
	<ul>
	<li>Eenmaal voor het hele Debian-archief</li>

	<li>Eenmaal voor elke officiële release (in gedachten houdend dat testing/unstable veranderen tussen releases)</li>

	<li>Eenmaal wanneer een nieuw programma met cryptografie aan het archief wordt toegevoegd</li>

	<li>Eenmaal wanneer een nieuwe versie van een programma met cryptografie wordt toegevoegd aan het archief.</li>
	</ul>

</blockquote>

<p>Ik denk dat u alleen een nieuwe kennisgeving moet indienen als u een nieuw programma toevoegt waarin cryptografie is verwerkt.  Updates van bestaande programma's moeten vallen onder de ruime formulering van de kennisgeving die wij hierboven hebben voorgesteld.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Nieuwe pakketten komen het debian-archief binnen via de volgende reeks stappen. Op welk moment moet de kennisgeving gebeuren?
</p>
	<ol>
	<li>Een bovenstroomse ontwikkelaar geeft een pakket vrij als openbron. Deze stap wordt overgeslagen in het geval van een eigen Debian-pakket.</li>
	<li>Een Debian-ontwikkelaar verpakt de broncode en het binaire bestand voor Debian, vaak met wijzigingen aan de broncode.</li>
	<li>Het pakket wordt geüpload naar ftp-master, incoming (inkomend).</li>
	<li>Het nieuwe pakket kan niet worden geïnstalleerd, omdat het nieuw is.</li>
	<li>Ftp-beheerders voegen de benodigde registers voor het pakket toe.</li>
	<li>Het pakket wordt na enkele dagen in het archief geïnstalleerd.</li>
	<li>Het pakket wordt gekopieerd naar de spiegelserversites.</li>
	</ol>
</blockquote>

<p>De regelgeving is vrij duidelijk dat de kennisgeving moet plaatsvinden vóór of tegelijk met de openbare beschikbaarheid. Voor uitvoer voorafgaand aan publieke beschikbaarheid is een uitvoervergunning nodig. Daarom, als het archief in stap 3 niet openbaar beschikbaar is, dan moet ofwel het pakket openbaar beschikbaar worden gemaakt vóór stap 3 (en kennisgevingen worden verstuurd), ofwel zullen exportlicenties nodig zijn voor Debian-ontwikkelaars.  Als het archief in stap 3 openbaar beschikbaar is, dan zou kennisgeving op dat moment de noodzaak van exportlicenties voor Debian-ontwikkelaars wegnemen.
</p>


<blockquote class="question">
<p>
<span>D:</span>
	Als de bovenstroomse auteur BXA op de hoogte heeft gebracht, is die kennisgeving dan nodig? (Het verpakken voor Debian kan wijzigingen aan de broncode met zich meebrengen met betrekking tot bestandslocaties, en soms functionele verschillen, hoewel het algemene doel is om de bovenstroomse code in Debian te laten werken met minimale aanpassingen).
</p>
</blockquote>

<p>Als de bovenstroomse auteur BXA op de hoogte heeft gebracht, is dat voldoende.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Moeten we kennisgeving indienen wanneer nieuwe binaire bestanden (objectcode) worden toegevoegd als we al een kennisgeving hebben ingediend voor de broncode?
</p>
</blockquote>

<p>Ik denk niet dat u een nieuwe kennisgeving moet indienen voor de objectcode, op voorwaarde dat een kennisgeving voor de broncode is ingediend.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Is kennisgeving vereist voor programma's die geen crypto-algoritmen bevatten, maar gekoppeld zijn aan cryptobibliotheken? Hoe zit het met programma's die andere programma's starten om cryptografische functies uit te voeren?
</p>
</blockquote>

<p>Zolang het programma openbron is, kan het een open crypto-API bevatten en toch in aanmerking komen voor licentie-uitzondering TSU.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Nieuwe programma's kunnen gemakkelijk worden gecontroleerd voordat ze worden uitgebracht (en de kennisgeving kan op dat moment gedaan worden), maar wanneer een update wordt uitgevoerd, is er geen handmatige stap om de kennisgeving te doen. Zou het aanvaardbaar zijn om BXA op de hoogte te stellen voor elke toevoeging van software, met een indicatie dat toekomstige updates de toevoeging van cryptofunctionaliteit kunnen bevatten?
</p>
</blockquote>

<p>Ja. Overrapportage moet waarschijnlijk worden vermeden waar dat redelijk is, maar onderrapportage moet worden vermeden. Voor toekomstige aanpassingen van een bestaand programma is geen afzonderlijke kennisgeving vereist. Alleen voor nieuwe programma's is afzonderlijke kennisgeving vereist.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Kunnen we het verzenden van kennisgevingen automatiseren?
</p>
</blockquote>

<p>U kunt het proces van het verzenden van kennisgevingen automatiseren. Dit is een interne procedurekwestie. BXA en NSA geven er niet om hoe u de indiening van kennisgevingen intern afhandelt.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Welke vorm moet de kennisgeving aannemen?
</p>
</blockquote>

<p>De kennisgeving aan BXA kan elektronisch of op papier gebeuren; de kennisgeving aan NSA moet echter op papier gebeuren.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Wie kan de kennisgevingen insturen? Moeten men bijvoorbeeld een Amerikaans staatsburger zijn?
</p>
</blockquote>

<p>Iedereen kan de kennisgeving indienen; staatsburgerschap is niet relevant.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Zijn er nog andere bekommernissen waar we rekening mee moeten houden?  Welke andere stappen dan kennisgeving moeten we nemen?
</p>
</blockquote>

<p>Behalve kennisgeving kunt u overwegen een omgekeerde IP-controle toe te passen die de computer identificeert die de download aanvraagt, en die downloads van het cryptografisch archief naar landen onder embargo van de Verenigde Staten blokkeert:  Cuba, Iran, Irak, Libië, Noord-Korea, Syrië, Soedan en het door de Taliban bezette Afghanistan. Daarnaast kunt u overwegen een bepaling in uw licentieovereenkomst op te nemen, of een apart scherm voorafgaand aan het downloaden, waarin de persoon die de software downloadt als volgt wordt geadviseerd:</p>

<p>Deze software is onderworpen aan Amerikaanse exportcontroles die van toepassing zijn op open source software die encryptie bevat.  Debian heeft de kennisgeving ingediend bij het Bureau voor exportadministratie en het Nationaal veiligheidsagentschap die vereist is voorafgaand aan de export onder de bepalingen van licentie-uitzondering TSU van de regelgeving van de Amerikaanse exportadministratie. In overeenstemming met de vereisten van Licentie-uitzondering TSU verklaart en garandeert u dat u in aanmerking komt om deze software te ontvangen, dat u zich niet bevindt in een land waarop een embargo rust van de Verenigde Staten en dat u de software niet direct of indirect zult gebruiken voor het ontwerpen, ontwikkelen, opslaan of gebruiken van nucleaire, chemische of biologische wapens of raketten. Gecompileerde binaire code die gratis wordt weggegeven, mag opnieuw worden uitgevoerd onder de bepalingen van licentie-uitzondering TSU. Voor commerciële producten die deze code bevatten, kunnen echter aanvullende technische controles en andere eisen gelden voordat zij uit de Verenigde Staten worden uitgevoerd. Zie voor aanvullende informatie www.bxa.doc.gov.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Momenteel kunnen gebruikers over de hele wereld toegang krijgen tot software die nog in ons archief moet worden opgenomen, en deze mogelijk downloaden. Waarschijnlijk zullen wij de nodige kennisgevingen doen wanneer software behandeld wordt om deze naar het archief over te brengen, waardoor dus software in deze staat nog wacht op kennisgeving. Zou dit een probleem zijn?  Zo ja, zou het acceptabel zijn om een alternatieve wachtrij van cryptografische software op te zetten die wacht op integratie in het archief en die alleen beschikbaar is voor onze ontwikkelaars?  Om software in onze distributie te kunnen opnemen, moeten ontwikkelaars, die zich vaak buiten de VS bevinden, de software kunnen onderzoeken en ervoor zorgen dat deze aan bepaalde richtlijnen voldoet. Hoe moeten we deze toegang bewerkstelligen?  Zouden we andere oplossingen voor dit aan de kennisgeving voorafgaand gebied van het archief moeten overwegen?
        </p>
	<p>Een probleem waar we vaak tegenaan lopen zijn softwareoctrooien. Het is duidelijk dat de integratie van cryptografie in software geen van de octrooiproblemen wegneemt waarover we normaal moeten nadenken. Zijn er echter nieuwe kwesties waarmee we rekening moeten houden wanneer octrooien in wisselwerking staan met de exportregels voor cryptografie? Het lijkt erop dat tenminste voor de uitzondering TSU (sectie 740.13 van de EAR), octrooien geen invloed hebben op de vraag of de broncode al dan niet openbaar is.

</p>
</blockquote>

<p>Het is belangrijk een onderscheid te maken tussen het archief dat voorwerp van kennisgeving is geweest en nieuwe programma's. U kunt het archief dat voorwerp van kennisgeving is geweest, bijwerken zonder verdere kennisgeving, zoals hierboven beschreven. Alleen voor nieuwe programma's is een aparte kennisgeving nodig, voordat deze worden geplaatst. Als nieuwe programma's moeten worden beoordeeld door ontwikkelaars voordat ze worden geplaatst, en dergelijke software is niet zowel openbaar beschikbaar als reeds aangemeld bij de Amerikaanse overheid, dan raad ik u aan te overwegen een exportvergunning aan te vragen die deze beperkte beoordeling vóór aanmelding toestaat. U hebt gelijk dat octrooien er niet toe leiden dat software niet in aanmerking komt voor export onder licentie-uitzondering TSU.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Distributie, spiegeling en cd's</p>

	<p>Moeten onze spiegelservers in de VS het BXA op de hoogte brengen als we cryptografie aan ons archief toevoegen? Hoe vaak moeten ze BXA inlichten? We willen een situatie vermijden waarin spiegelservers een melding moeten doen voor elk nieuw programma dat Debian aan het archief toevoegt, zelfs als onze hoofdserver dergelijke meldingen moet doen. We moeten de bewerkingen voor spiegelserverbeheerders eenvoudig houden. Wat zouden spiegelservers buiten de VS eventueel moeten doen?</p>

	<p>Als we een update naar een spiegelserver sturen in plaats van te wachten tot deze de software downloadt, moeten we dan speciale stappen ondernemen?  Wat als we een spiegelserver een verzoek sturen om nieuwe/gewijzigde software te downloaden?

</p>
</blockquote>

<p>Zodra de kennisgeving voor de centrale server is ingediend, is geen verdere kennisgeving vereist voor spiegelserversites.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Welke van de volgende (eventuele) verkopers zouden ongewijzigde Debian-binaire bestanden (en broncode) kunnen verzenden met alleen kennisgeving? Welke zouden beoordeling en goedkeuring vereisen? Kan de beoordeling gelijktijdig met de verzending plaatsvinden, of moet de goedkeuring voorafgaan aan de verzending?
</p>
	<p>
	A) postorderverzending van cd's voor de kosten van de media?<<br />
	B) postorderverzending van cd's met winstoogmerk?<br />
	C) rechtstreekse verkoop uit voorraad van cd's voor de kosten van de media?<br />
	D) rechtstreekse verkoop uit voorraad van cd's met winstoogmerk?<br />
	E) verkoper die cd's van A of C hierboven levert, samen met hardware.  HW verkocht met winst, maar zonder vooraf geïnstalleerde software?<br />
	F) E, maar met vooraf geïnstalleerde software?<br />
	G) een van de bovenstaande, verkoopsondersteuning voor de software?
	</p>

	<p>Als het eenvoudiger is, is een andere manier om dit te bekijken: aan welke voorwaarden moet de verkoper voldoen om binaire bestanden te verzenden onder licentie-uitzondering TSU, en voor welke uitgaven mag de verkoper de kosten terugvorderen en/of op welke kosten mag deze winst boeken?</p>
</blockquote>

<p>Redelijke en gebruikelijke vergoedingen voor reproductie en distributie zijn toegestaan, maar geen licentievergoedingen of royalty's. Ondersteuning is ook toegestaan, met inachtneming van de bovenstaande beperking.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Als de eenmalige beoordeling vereist is voor ongewijzigde binaire bestanden die met winstoogmerk worden verzonden, kan die goedkeuring dan worden gebruikt door andere verkopers die ongewijzigde binaire bestanden verzenden?
</p>
</blockquote>

<p>Een eenmalige beoordeling is voor het product en is leveranciersonafhankelijk.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Zou het acceptabel zijn om een officiële spiegelserver op te zetten in een land dat verboden is volgens EAR sectie 740.13(e)?
</p>
</blockquote>

<p>U zou een vergunning moeten aanvragen om een officiële spiegelserver op te zetten in een land dat onder embargo staat.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Als het technisch niet mogelijk is de toegang vanuit de T7-landen tot een web- (of ftp-, enz.) server te blokkeren, vereisen de zorgvuldigheidsvereisten dan extreme maatregelen?  Voldoet de de facto standaard van de (Amerikaanse) industrie aan de zorgvuldigheidsvereisten?
</p>
</blockquote>

<p>De de facto industriestandaard zou moeten volstaan. Ik hoop dat de regering inziet dat elk door de mens bedacht systeem met voldoende inspanning kan worden verslagen.</p>


<blockquote class="question">
<p>
<span>D:</span>
	Welke stappen moeten we nemen als we merken dat iemand software downloadt naar één van deze landen vanaf een spiegelserver in de VS? Wat als we merken dat iemand software downloadt naar een van deze landen vanaf een spiegelserver buiten de VS?
</p>
	<p>Sommige van onze ontwikkelaars wonen mogelijk in of zijn burgers van de zeven landen waarvoor TSU-vrijstelling verboden is. Zou het een probleem zijn voor deze ontwikkelaars om toegang te hebben tot cryptografische software op onze machines? Moeten we hen vragen dergelijke software niet te downloaden? Welke stappen moeten we nemen als we ontdekken dat ze cryptografische software downloaden?</p>
</blockquote>

<p>Het louter plaatsen van cryptografische software op een server die toegankelijk kan zijn vanuit een land waarop een embargo rust, houdt geen <q>kennis</q> in van het feit dat de software daarheen is uitgevoerd. Daarom is strafrechtelijke aansprakelijkheid niet van toepassing op dit plaatsen. Wij raden u aan IP-controles uit te voeren en downloads naar landen waarvan  bekend is dat zij onder embargo staan, te weigeren. Deze zorgvuldigheid is ook een verdediging tegen een vordering van wettelijke aansprakelijkheid. Als u erachter komt dat uw software is gedownload naar een verboden bestemming, raad ik u aan toekomstige downloads naar die specifieke site te blokkeren, tenzij en totdat u een licentie van BXA verkrijgt.</p>

<hr />

<p>Debian dankt de <a href="http://www.hp.com/">Hewlett-Packard</a>
<a href="http://www.hp.com/products1/linux/">Linux Systems Operation</a>
voor hun steun bij het verkrijgen van dit juridisch advies.</p>
