#use wml::debian::template title="Debian Documentatieproject (DDP)" MAINPAGE="true"
#use wml::debian::translation-check translation="a63b4ba503c2c880b94edc8f521b5d8f769fa2c6"

# Translator: $Author$
# Last update: $Date$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#ddp_work">Ons werk</a></li>
</ul>


<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Het Debian Documentatieproject (DDP) zorgt voor de documentatie van Debian, bijvoorbeeld de handleidingen voor gebruikers en ontwikkelaars, verschillende handboeken, FAQ's en notities bij de release. Deze pagina biedt een kort overzicht van het werk van het DDP.</p>
</aside>

<p>Het Debian Documentatie Project heeft als doel alle inspanningen om meer
en betere documentatie voor Debian te schrijven, te coördineren en samen te
bundelen.</p>

<h2><a id="ddp_work">Ons werk</h2>

<p>
<div class="line">
  <div class="item col50">

    <h3>Handleidingen</h3>
    <ul>
	  <li><strong><a href="user-manuals">Handleidingen voor
	      gebruikers</a></strong></li>
	  <li><strong><a href="devel-manuals">Handleidingen voor
	      ontwikkelaars</a></strong></li>
      <li><strong><a href="misc-manuals">Diverse handleidingen</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Documentatiebeleid</h3>
    <ul>
      <li>De licenties van de handleidingen moeten voldoen aan de DFSG.</li>
	  <li>We gebruiken reStructuredText of Docbook XML (verouderd) voor onze documenten.</li>
      <li>Alle broncode is beschikbaar in onze <a href="https://salsa.debian.org/ddp-team">Git-opslagplaats</a>.</li>
      <li><tt>www.debian.org/doc/&lt;naam-van-de-handleiding&gt;</tt> is de officiële URL.</li>
      <li>Elk document moet actief onderhouden worden.</li>
    </ul>

    <h3>Git-toegang</h3>
    <ul>
	  <li>Toegang krijgen tot <a href="vcs">de git-opslagplaats van
	      het DDP</a></li>
    </ul>

  </div>

</div>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> Neem contact op met het team op <a href="https://lists.debian.org/debian-doc/">debian-doc</a></button></p>

