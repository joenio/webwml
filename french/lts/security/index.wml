#use wml::debian::translation-check translation="f09aa0d33f340a6365546d93fa2ee30165632f8f" maintainer="Jean-Paul Guillonneau"
#use wml::debian::template title="LTS — Informations de sécurité" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Garder son système Debian LTS sécurisé</toc-add-entry>

<p>Pour obtenir les dernières informations de sécurité de Debian LTS, abonnez-vous
à la liste de diffusion <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.
</p>

<p>Pour plus d’informations sur les problèmes de sécurité dans Debian LTS, veuillez
consulter les <a href="../../security">informations de sécurité de Debian</a>.
</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Annonces récentes</toc-add-entry>

<p>Ces pages web contiennent une archive condensée des annonces de sécurité qui
sont postées sur la liste de diffusion
<a href="https://lists.debian.org/debian-lts-announce/"></a>.
</p>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}
<p>Les dernières annonces de sécurité de Debian LTS sont aussi disponibles au
<a href="dla">format RDF</a>. Nous proposons aussi un
<a href="dla-long">second fichier</a> qui inclut le premier paragraphe de
l’annonce de sécurité correspondante, de façon à connaître le sujet de
l’annonce.
</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Les anciennes annonces de sécurité sont également disponibles :
<:= get_past_sec_list(); :>

<!-- This section a copy from /security/dsa.wml. TODO: create and include file -->
<toc-add-entry name="infos">Sources d’informations de sécurité</toc-add-entry>

<ul>
<li><a href="https://security-tracker.debian.org/">Debian Security Tracker</a>,
source principale pour toutes les informations relatives à la sécurité, avec
options de recherche</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">Liste JSON</a>
contenant des descriptions de CVE, des noms de paquet, des numéros de bogue,
des versions de paquet avec correctif, sans aucun DSA</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">Liste de DSA</a>
contenant des DSA avec la date, les numéros de CVE relatifs, les versions de
paquet avec correctif</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">Liste de DLA</a>
contenant des DLA avec la date, les numéros de CVE relatifs, les versions de
paquet avec correctif</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
Annonces de DSA</a></li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
Annonces de DLA</a></li>

<li><a href="oval">Fichiers Oval</a></li>

<li>Recherche de DSA (les capitales sont importantes)<br>
p.ex., <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Recherche de DLA (-1 est important)<br>
p.ex., <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Recherche de CVE<br>
p.ex., <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>

<ul>
<li> <a href="https://lts-team.pages.debian.net/wiki/FAQ">FAQ de Debian LTS</a>,
une réponse à votre question peut déjà s’y trouver !
<li>
<a href="https://lts-team.pages.debian.net/wiki/Contact">Information pour
contacter l’équipe Debian LTS</a>
</ul>
