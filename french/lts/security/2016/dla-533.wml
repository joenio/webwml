#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5093">CVE-2016-5093</a>

<p>L'absence de caractère null provoque une longueur de zend_string
inattendue et une fuite mémoire de tas. Le script de test utilise
locale_get_primary_language pour accéder à get_icu_value_internal mais il y
a quelques autres fonctions qui déclenchent aussi ce problème :<br>
locale_canonicalize, locale_filter_matches,<br>
locale_lookup, locale_parse</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5094">CVE-2016-5094</a>

<p>pas de création de chaînes avec des longueurs en dehors de l'intervalle
des entiers</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5095">CVE-2016-5095</a>

<p>similaire au <a href="https://security-tracker.debian.org/tracker/CVE-2016-5094">CVE-2016-5094</a>
pas de création de chaînes avec des longueurs en dehors de l'intervalle des
entiers</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5096">CVE-2016-5096</a>

<p>confusion int/size_t dans fread</p></li>

<li>CVE-TEMP-bug-70661

<p>bogue nº 70661: vulnérabilité d’utilisation de mémoire après libération
dans la désérialisation de paquet de WDDX</p></li>

<li>CVE-TEMP-bug-70728

<p>bogue nº 70728 : vulnérabilité de confusion de type dans
PHP_to_XMLRPC_worker()</p></li>

<li>CVE-TEMP-bug-70741

<p>bogue nº 70741 : vulnérabilité de confusion de type dans la
désérialisation de paquet de session WDDX</p></li>

<li>CVE-TEMP-bug-70480-raw

<p>bogue nº 70480 : dépassement de tampon en lecture dans
php_url_parse_ex()</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.4.45-0+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-533.data"
# $Id: $
