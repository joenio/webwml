#use wml::debian::translation-check translation="2d8aa8ce4c88e03582c09befc9ad03c6b190bfbf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans la bibliothèque TIFF (Tag
Image File Format) et ses outils associés.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11335">CVE-2017-11335</a>

<p>Une image PlanarConfig=Contig peut provoquer une écriture hors limites
(relative à la fonction ZIPDecode). Une entrée contrefaite pourrait conduire
à une attaque distante par déni de service ou à une attaque par exécution de code
arbitraire.</p>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12944">CVE-2017-12944</a>

<p>Une mauvaise gestion d’allocation mémoire pour de petits fichiers permet
à des attaquants de provoquer un déni de service (échec d’allocation et plantage
d'application) lors de l’invocation de tiff2pdf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13726">CVE-2017-13726</a>

<p>Une assertion abort disponible permet à une entrée contrefaite de conduire
une attaque distante par déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13727">CVE-2017-13727</a>

<p>Une assertion abort disponible permet à une entrée contrefaite de conduire
une attaque distante par déni de service.</p></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.0.2-6+deb7u16.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1093.data"
# $Id: $
