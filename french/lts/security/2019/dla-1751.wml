#use wml::debian::translation-check translation="0c9b25d57948cbe327556f9a924d5603d3b7abdc" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans suricata, le moteur de
détection de menaces pour le réseau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10242">CVE-2018-10242</a>

<p>Absence de vérification de longueur causant une lecture hors limites dans
SSHParseBanner (app-layer-ssh.c). Des attaquants distants peuvent exploiter
cette vulnérabilité pour provoquer un déni de service ou éventuellement une
divulgation non autorisée d’informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10243">CVE-2018-10243</a>

<p>Terminaison inattendue du champ Authorization causant une lecture hors
limites de tampon basé sur le tas dans htp_parse_authorization_digest
(htp_parsers.c, de la copie embarquée de LibHTP). Des attaquants distants
peuvent exploiter cette vulnérabilité pour provoquer un déni de service ou
éventuellement une divulgation non autorisée d’informations.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.0.7-2+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets suricata.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1751.data"
# $Id: $
