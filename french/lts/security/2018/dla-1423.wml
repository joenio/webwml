#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Linux 4.9 a été empaqueté pour Debian 8 sous linux-4.9. Cela fournit un
chemin de gestion de mise à niveau pour des systèmes qui actuellement utilisent
les paquets de noyau de la suite « Jessie-backports ».</p>

<p>Il n’est nullement nécessaire de mettre à niveau les systèmes utilisant
Linux 3.16, car cette version de noyau continuera d’être prise en charge
pendant la période LTS.</p>

<p>Ce rétroportage n’inclut pas les paquets binaires suivants :</p>

<p>hyperv-daemons, libcpupower1, libcpupower-dev, libusbip-dev,
linux-compiler-gcc-4.9-x86, linux-cpupower linux-libc-dev usbip.</p>

<p>Les versions plus anciennes de la plupart de ceux-ci sont construites
à partir d’autres paquets source dans Debian 8.</p>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui pourraient
conduire à une augmentation de droits, un déni de service ou une fuite
d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5753">CVE-2017-5753</a>

<p>De nouvelles instances de code vulnérables à Spectre variante 1
(contournement de vérification de limites) ont été mitigées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18255">CVE-2017-18255</a>

<p>Le sous-système de réalisation d’évènements ne validait pas correctement la
valeur de sysctl kernel.perf_cpu_time_max_percent sysctl. Le réglage d’une
grande valeur pourrait avoir un impact de sécurité non précisé. Cependant, seul
un utilisateur privilégié peut régler cette sysctl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1118">CVE-2018-1118</a>

<p>Le logiciel syzbot a découvert que le pilote vhost n’initialisait pas les
tampons de message, qui pouvaient plus tard être lus par des processus utilisateur.
Un utilisateur avec accès au périphérique /dev/vhost-net pourrait utiliser cela
pour lire des informations sensibles du noyau ou d’autres processus
d’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1120">CVE-2018-1120</a>

<p>Qualys a signalé qu’un utilisateur capable de monter des systèmes de fichiers
FUSE peut créer un processus de manière qu’un autre processus essayant de lire
sa ligne de commande sera bloqué pendant un temps long arbitraire. Cela pourrait
être utilisé pour un déni de service, ou aider à exploiter une situation de
compétition dans l’autre programme.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1130">CVE-2018-1130</a>

<p>Le logiciel syzbot a trouvé que l’implémentation de DCCP de sendmsg() ne
vérifie pas l’état du socket, aboutissant éventuellement à un déréférencement de
pointeur NULL. Un utilisateur local pourrait utiliser cela pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a>

<p>Plusieurs chercheurs ont découvert que SSB (Speculative Store Bypass), une
fonction implémentée dans beaucoup de processeurs, pourrait être utilisé pour
lire des informations sensibles d’autre contexte. En particulier, du code dans
un bac à sable logiciel pourrait lire des informations sensibles en dehors
du bac à sable. Ce problème est aussi connu comme Spectre variante 4.</p>

<p>Cette mise à jour permet de mitiger ce problème sur quelques processeurs x86
en désactivant SSB. Cela nécessite une mise à jour du micrologiciel qui n’est
pas libre. Cela pourra être inclus dans une mise à jour du système BIOS ou du
micrologiciel UEFI, ou dans une future mise à jour des paquets intel-microcode
ou amd64-microcode.</p>

<p>Désactiver SSB peut réduire les performances de manière significative, aussi,
par défaut, cela est réalisé que dans les tâches qui utilisent la fonction
seccomp. Les applications qui veulent cette mitigation doivent la demander
explicitement à travers l’appel système prctl(). Les utilisateurs peuvent
contrôler où cette mitigation est activée avec le paramètre
spec_store_bypass_disable du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5814">CVE-2018-5814</a>

<p>Jakub Jirasek a signalé une situation de compétition dans le pilote hôte
USB/IP. Un client malveillant pourrait utiliser cela pour provoquer un déni de
service (plantage ou corruption de mémoire), et éventuellement exécuter du
code, sur le serveur USB/IP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10021">CVE-2018-10021</a>

<p>Un attaquant physiquement présent débranchant un câble SAS peut causer un
déni de service (fuite de mémoire et WARN).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10087">CVE-2018-10087</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10124">CVE-2018-10124</a>

<p>zhongjiang a trouvé que l’implémentation d’appels système wait4() et
kill() ne vérifiait pas si une valeur de pid de INT_MIN était valable. Si un
utilisateur passait une telle valeur, le comportement du code était formellement
non défini et pouvait avoir un impact de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10853">CVE-2018-10853</a>

<p>Andy Lutomirski et Mika Penttilä ont signalé que KVM pour les processeurs x86
ne réalisait pas la vérification de privilèges nécessaires lors de de l’émulation
de certaines instructions. Cela pourrait être utilisé par un utilisateur non
privilégié dans une VM de système invité pour augmenter ses droits dans le
système invité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10876">CVE-2018-10876</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10877">CVE-2018-10877</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10878">CVE-2018-10878</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10879">CVE-2018-10879</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10880">CVE-2018-10880</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10881">CVE-2018-10881</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10882">CVE-2018-10882</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10883">CVE-2018-10883</a>

<p>Wen Xu de SSLab, Gatech, a signalé que des images contrefaites de système de
fichiers ext4 pourraient déclencher un plantage ou une corruption de mémoire. Un
utilisateur local capable de monter des systèmes de fichiers arbitraires ou un
attaquant fournissant des systèmes de fichiers à monter, pourraient utiliser cela
pour un déni de service ou éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10940">CVE-2018-10940</a>

<p>Dan Carpenter a signalé que le pilote de disque optique (cdrom) ne validait
pas correctement le parametre pour l’ioctl CDROM_MEDIA_CHANGED. Un utilisateur
ayant accès à un périphérique cdrom pourrait utiliser cela  pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11506">CVE-2018-11506</a>

<p>Piotr Gabriel Kosinski et Daniel Shapira ont signalé que le pilote de disque
optique SCSI (sr) n’allouait pas un tampon suffisamment grand pour les données
indicatives « sense ». Un utilisateur, ayant accès à un périphérique optique SCSI
pouvant produire plus le 64 octets de données indicatives, pourrait utiliser cela
pour provoquer un déni de service (plantage or corruption de mémoire), et
éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12233">CVE-2018-12233</a>

<p>Shankara Pailoor a signalé qu’une image de système de fichiers JFS contrefaite
pourrait déclencher un déni de service (corruption de mémoire). Cela pourrait
éventuellement être aussi utilisé pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000204">CVE-2018-1000204</a>

<p>Le logiciel syzbot a trouvé que le pilote générique SCSI (sg) pouvait en
certaines circonstances permettre de lire des données de tampons non initialisés.
qui pourraient inclure des informations sensibles du noyau ou d’autres tâches.
Cependant, seul les utilisateurs privilégiés avec les capacités CAP_SYS_ADMIN ou
CAP_SYS_RAWIO étaient autorisés à cela, donc l’impact de sécurité était faible
ou inexistant.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.110-1~deb8u1. De plus, cette mise à jour corrige les bogues
Debian n° 860900, n° 872907, n° 892057, n° 896775, n° 897590, et n° 898137, et
inclut beaucoup d’autres corrections de bogues issues des mises à jour des
versions 4.9.89 à 4.9.110 incluses.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1423.data"
# $Id: $
