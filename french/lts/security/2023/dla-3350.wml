#use wml::debian::translation-check translation="076795ffb2183dd30fd56439ae005b38fdb5ef8f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>node-css-what était vulnérable à un déni de service par expression
rationnelle (ReDoS) à cause de l’utilisation d’une expression rationnelle non
sûre dans la variable re_attr. L’exploitation de cette vulnérabilité pouvait
être faite à l’aide de la fonction parse.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.1.0-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-css-what.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-css-what,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-css-what">\
https://security-tracker.debian.org/tracker/node-css-what</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3350.data"
# $Id: $
