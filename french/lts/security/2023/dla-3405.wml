#use wml::debian::translation-check translation="aaf43b8b6128d792cbea9944e13ecab77ecb0150" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans libxml2, une bibliothèque
bibliothèque pour lire, modifier ou écrire des fichiers XML et HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28484">CVE-2023-28484</a>

<p>Un défaut de déréférencement de pointeur NULL lors de l’analyse de schémas
XML non valables pouvait aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29469">CVE-2023-29469</a>

<p>Il a été signalé que lors du hachage de chaines vides non terminées par NULL,
 xmlDictComputeFastKey pouvait produire des résultats incohérents, ce qui
pouvait conduire à diverses erreurs de logique ou de mémoire.</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.9.4+dfsg1-7+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxml2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxml2">\
https://security-tracker.debian.org/tracker/libxml2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3405.data"
# $Id: $
