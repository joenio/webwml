#use wml::debian::translation-check translation="621b03a84eafaa5a3e1533cade2f5a0059d51c73" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans HDF5, un
ensemble de formats de fichiers et une bibliothèque pour des données
scientifiques. Des fuites de mémoire, des lectures hors limites et des erreurs
de division par zéro pouvaient conduire à un déni de service lors du traitement
d’un fichier HDF mal formé.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.10.4+repack-10+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets hdf5.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de hdf5,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/hdf5">\
https://security-tracker.debian.org/tracker/hdf5</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3522.data"
# $Id: $
