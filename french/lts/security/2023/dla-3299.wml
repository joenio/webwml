#use wml::debian::translation-check translation="b667a7e78ea30941c9f77a77cf5642a7b95e20f9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Nathanael Braun et Johan Brissaud ont découvert une vulnérabilité
d’empoisonnement de prototype dans node-qs, un module de Node.js pour analyser
et transformer les chaines de requêtes. node-qs, versions 6.5.x avant 6.5.3
permettait, par exemple, la création d’objets de type tableau en définissant un
tableau dans la propriété <code>__ proto__</code>. Les objets résultants
héritaient du prototype <code>Array</code>, par conséquent exposant les
fonctions natives Array.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 6.5.2-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-qs.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-qs,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-qs">\
https://security-tracker.debian.org/tracker/node-qs</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3299.data"
# $Id: $
