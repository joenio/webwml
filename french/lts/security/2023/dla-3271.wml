#use wml::debian::translation-check translation="1cd050b43fb285723958730aaf0e8b43a05a9356" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de déni de service par expression rationnelle (ReDoS) a été
découverte dans node-minimatch, un module de Node.js utilisé pour convertir des
expressions de modèle global en objets RegExp, qui pouvait aboutir à un déni de
service lors de l’appel de la fonction <code>braceExpand()</code> avec des
arguments spécifiques.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.0.4-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets node-minimatch.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de node-minimatch,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/node-minimatch">\
https://security-tracker.debian.org/tracker/node-minimatch</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3271.data"
# $Id: $
