#use wml::debian::translation-check translation="f0257f9e02e557ea34fb1c227c77b4aa45bea08b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans activemq, un courtier
de message construit autour de Java Message Service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15709">CVE-2017-15709</a>

<p>Lors de l’utilisation du protocole OpenWire dans activemq, il a été découvert
que certains détails du système (tels que le système d’exploitation et la
version du noyau) sont exposés en texte simple.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11775">CVE-2018-11775</a>

<p>La vérification du nom d’hôte TLS lors de l’utilisation du client ActiveMQ
d’Apache est absente, ce qui pourrait rendre le client vulnérable à une attaque
de l’homme du milieu entre l’application Java utilisant le client ActiveMQ et
le serveur  ActiveMQ. Cette vérification est désormais activée par défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0222">CVE-2019-0222</a>

<p>Une déconversion de paramètre de trame MQTT corrompue peut conduire
à négocier une exception d’épuisement de mémoire la rendant inactive.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26117">CVE-2021-26117</a>

<p>Le module facultatif d’ActiveMQ de connexion LDAP peut être configuré pour
utiliser un accès anonyme au serveur LDAP. Le contexte d’anonymisation est
utilisé pour vérifier un mot de passe valable lors d’erreur, aboutissant
à aucune vérification du mot de passe.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 5.14.3-3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets activemq.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de activemq, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/activemq">\
https://security-tracker.debian.org/tracker/activemq</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2583.data"
# $Id: $
