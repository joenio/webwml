#use wml::debian::translation-check translation="49c8065bc71a68e4ab23d992dcd1df3326d3c75d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les CVE suivants ont été signalés à l’encontre de src:intel-microcode.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>

<p>Une nouvelle attaque de contournement de domaine par exécution transitoire
connue comme SRBDS (Special Register Buffer Data Sampling) a été découverte. Ce
défaut permet que les valeurs de données dans des registres internes spéciaux
peuvent être divulguées par un attaquant capable d’exécuter du code sur
n’importe quel cœur du CPU. Un attaquant local, sans privilèges particuliers,
peut utiliser ce défaut pour inférer des valeurs renvoyées par les instructions
affectées, connues pour être couramment utilisées lors des opérations de
chiffrement qui reposent sur l’unicité, la confidentialité ou les deux à la fois.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0548">CVE-2020-0548</a>

<p>Un défaut a été découvert dans les processeurs Intel par lequel un attaquant
local est capable d’obtenir des informations sur des registres utilisés pour
des calculs de vecteur en observant l’état de registres d’autres processus en
cours d’exécution sur le système. Cela conduisait à une situation de compétition
où des tampons de stockage, qui n’étaient pas nettoyés, pourraient être lus par
un autre processus ou CPU voisin. Le plus grand danger de cette
vulnérabilité est la confidentialité des données lorsqu’un attaquant peut lire
des données arbitraires lors de leur passage dans le processeur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0549">CVE-2020-0549</a>

<p>Un défaut temporel de microarchitecture a été découvert sur certains
processeurs d’Intel. Un cas particulier existe dans lequel des données en
transit lors du processus d’expulsion peuvent finir dans les tampons « fill »
et n’être pas correctement nettoyées lors des mitigations MDS. Le contenu de
ces tampons (qui étaient attendus vierges) peut être déduit en utilisant des
méthodes d’attaque de style MDS ou TAA pour permettre à un attaquant local
de présumer les valeurs de ces tampons.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.20200609.2~deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2248.data"
# $Id: $
