#use wml::debian::translation-check translation="52f4fb599cba858fc6011d2bbdd9ce2bac8d0c18" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités d'analyse de fichier ont été corrigées dans
libraw. Elles concernent les formats dng et x3f.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35530">CVE-2020-35530</a>

<p>Il y a une vulnérabilité de lecture hors limites, rapportée par
l'utilisateur de GitHub 0xfoxone, dans la fonction <q><code>new_node()</code></q>
(<code>src/x3f/x3f_utils_patched.cpp</code>) qui peut être déclenchée à
l'aide d'un fichier X3F contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35531">CVE-2020-35531</a>

<p>Une vulnérabilité de lecture hors limites, rapportée par l'utilisateur
de GitHub GirlElecta, existe dans la fonction <q><code>get_huffman_diff()</code></q>
(<code>src/x3f/x3f_utils_patched.cpp</code>) lors de la lecture d'un
fichier d'image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35532">CVE-2020-35532</a>

<p>Une vulnérabilité de lecture hors limites, rapportée par l'utilisateur
de GitHub GirlElecta, existe dans la fonction <q><code>simple_decode_row()</code></q>
(<code>src/x3f/x3f_utils_patched.cpp</code>) qui peut être déclenchée à
l'aide d'une image avec un grand champ <code>row_stride</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35533">CVE-2020-35533</a>

<p>Une vulnérabilité de lecture hors limites, rapportée par l'utilisateur
de GitHub GirlElecta, existe dans la fonction <q><code>LibRaw::adobe_copy_pixel()</code></q>
(<code>src/decoders/dng.cpp</code>) lors de la lecture d'un fichier
d'image.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
0.19.2-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libraw.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libraw, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libraw">\
https://security-tracker.debian.org/tracker/libraw</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3113.data"
# $Id: $
