#use wml::debian::translation-check translation="f1a98e6527dd2e7f9f5eff545bc701430b4f362f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans Curl, un outil en
ligne de commande et une bibliothèque côté client de transfert par URL
d'utilisation facile :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38545">CVE-2023-38545</a>

<p>Jay Satiro a découvert un dépassement de tampon dans l'initialisation de
connexion de mandataire SOCKS5.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-38546">CVE-2023-38546</a>

<p>Dans certaines circonstances libcurl était vulnérable à une injection de
cookies.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 7.74.0-1.3+deb11u10.</p>

<p>Pour la distribution stable (Bookworm), ces problèmes ont été corrigés
dans la version 7.88.1-10+deb12u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5523.data"
# $Id: $
