#use wml::debian::translation-check translation="ab2ee5ee4370f4409b48e90d62d268dfcaa25735" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans git, un système de
gestion de versions distribué, rapide et évolutif.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22490">CVE-2023-22490</a>

<p>yvvdwf a découvert une vulnérabilité d'exfiltration de données pendant
la réalisation d'un clonage local à partir d'un dépôt malveillant même en
utilisant un transport non local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23946">CVE-2023-23946</a>

<p>Joern Schneeweisz a découvert une vulnérabilité de traversée de
répertoires git-apply où un chemin en dehors de l'arbre de travail peut
être écrasé sous l'identité de l'utilisateur actif.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1:2.30.2-1+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets git.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de git, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5357.data"
# $Id: $
