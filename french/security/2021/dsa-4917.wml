#use wml::debian::translation-check translation="1fe97ede528cadc78447b1edeb9a753b00b0639c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30506">CVE-2021-30506</a>

<p>@retsew0x01 a découvert une erreur dans l'interface d'installation
d'applications Web.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30507">CVE-2021-30507</a>

<p>Alison Huffman a découvert une erreur dans le mode hors ligne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30508">CVE-2021-30508</a>

<p>Leecraso et Guang Gong ont découvert un problème de dépassement de
tampon dans l'implémentation de Media Feeds.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30509">CVE-2021-30509</a>

<p>David Erceg a découvert un problème d'écriture hors limites dans
l'implémentation de Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30510">CVE-2021-30510</a>

<p>Weipeng Jiang a découvert une situation de compétition dans le
gestionnaire de fenêtres Aura.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30511">CVE-2021-30511</a>

<p>David Erceg a découvert un problème de lecture hors limites dans
l'implémentation de Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30512">CVE-2021-30512</a>

<p>ZhanJia Song a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation des notifications.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30513">CVE-2021-30513</a>

<p>Man Yue Mo a découvert l'utilisation d'un type incorrect dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30514">CVE-2021-30514</a>

<p>koocola et Wang ont découvert un problème d'utilisation de mémoire après
libération dans la fonction Autofill.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30515">CVE-2021-30515</a>

<p>Rong Jian et Guang Gong ont découvert un problème d'utilisation de
mémoire après libération dans l'API d'accès au système de fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30516">CVE-2021-30516</a>

<p>ZhanJia Song a découvert un problème de dépassement de tampon dans
l'historique de navigation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30517">CVE-2021-30517</a>

<p>Jun Kokatsu a découvert un problème de dépassement de tampon dans le mode
lecture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30518">CVE-2021-30518</a>

<p>laural a découvert l'utilisation d'un type incorrect dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30519">CVE-2021-30519</a>

<p>asnine a découvert un problème d'utilisation de mémoire après libération
dans la fonction de paiement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30520">CVE-2021-30520</a>

<p>Khalil Zhani a découvert un problème d'utilisation de mémoire après
libération dans l'implémentation de Tab Strip.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 90.0.4430.212-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4917.data"
# $Id: $
