#use wml::debian::translation-check translation="f75654688f6e9dd06e68fca49b38b4d697ac6c1d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans Unbound, un serveur DNS de
mise en cache uniquement récursif ; une attaque par amplification de trafic
à l'encontre de serveurs de noms tiers faisant autorité (NXNSAttack) et une
validation insuffisante de réponses de serveurs amont pourraient avoir pour
conséquence un déni de service à l'aide d'une boucle infinie.</p>

<p>La version de Unbound dans la distribution oldstable (Stretch) n'est
plus prise en charge. Si ces problèmes de sécurité affectent votre
configuration, vous devriez mettre à niveau vers la distribution stable
(Buster).</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1.9.0-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets unbound.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de unbound, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/unbound">\
https://security-tracker.debian.org/tracker/unbound</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4694.data"
# $Id: $
