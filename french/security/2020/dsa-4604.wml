#use wml::debian::translation-check translation="73b976c71b8b4c13c331a478bd9111aa6f64627e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans cacti, un système de
supervision de serveur, avec éventuellement pour conséquence l'exécution de
code SQL ou la divulgation d'informations par des utilisateurs
authentifiés.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16723">CVE-2019-16723</a>

<p>Des utilisateurs authentifiés peuvent contourner les vérifications
d'autorisation pour afficher un graphique en soumettant des requêtes avec
des paramètres local_graph_id modifiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17357">CVE-2019-17357</a>

<p>L'interface d'administration de graphiques vérifie de façon insuffisante
le paramètre template_id, avec éventuellement pour conséquence une
injection SQL. Cette vulnérabilité pourrait être exploitée par des
attaquants authentifiés pour réaliser l'exécution non autorisée de code
SQL sur la base de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17358">CVE-2019-17358</a>

<p>La fonction sanitize_unserialize_selected_items (lib/fonctions.php)
vérifie de façon insuffisante les entrées de l'utilisateur avant de les
désérialiser, avec éventuellement pour conséquence une désérialisation non
sûre de données contrôlées par l'utilisateur. Cette vulnérabilité pourrait
être exploitée par des attaquants authentifiés pour influencer le flux de
contrôle du programme ou provoquer une corruption de mémoire.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 0.8.8h+ds1-10+deb9u1. Veuillez noter que Stretch était
seulement affectée par le
<a href="https://security-tracker.debian.org/tracker/CVE-2018-17358">CVE-2018-17358</a>.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1.2.2+ds1-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cacti.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cacti, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cacti">\
https://security-tracker.debian.org/tracker/cacti</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4604.data"
# $Id: $
