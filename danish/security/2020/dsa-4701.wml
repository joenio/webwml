#use wml::debian::translation-check translation="90833ca5169a5ef4cdeac320dd3d7016a5d5f8d2" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Denne opdatering indeholder opdateret CPU-microcode til nogle former for 
Intel-CPU'er, og leverer afhjælpning af hardwaresårbarhederne Special Register 
Buffer Data Sampling
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>),
Vector Register Sampling
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0548">CVE-2020-0548</a>)
og L1D Eviction Sampling
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0549">CVE-2020-0549</a>).</p>

<p>Microcode-opdateringen til HEDT- og Xeon-CPU'er med signature 0x50654, som 
var vendt om DSA 4565-2, leveres nu igen med et rettet udgivelse.</p>

<p>Opstrøms opdatering til Skylake-U/Y (signature 0x406e3) kunne ikke medtages i 
denne opdatering, på grund af rapporterede hængninger ved boot.</p>

<p>For flere oplysninger, se 
<a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html</a>,
<a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html</a>.</p>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 3.20200609.2~deb9u1.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 3.20200609.2~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine intel-microcode-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende intel-microcode, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4701.data"
