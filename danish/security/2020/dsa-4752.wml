#use wml::debian::translation-check translation="5346ad19e1bb39a2123f70e49de6fe4ffa9caa5b" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i BIND, en DNS-server-implementering.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8619">CVE-2020-8619</a>

    <p>Man opdagede at et asterisktegn i en tom ikke-terminal, kunne forårsage
    en assertionfejl, medførende lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8622">CVE-2020-8622</a>

    <p>Dave Feldman, Jeff Warren og Joel Cunningham rapporterede at et trunkeret 
    TSIG-svar, kunne føre til en assertionfejl, medførende 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8623">CVE-2020-8623</a>

    <p>Lyu Chiy rapporterede at en fejl i den indbyggede PKCS#11-kode, kunne 
    føre til en fjernudløsbar assertionfejl, medførende 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8624">CVE-2020-8624</a>

    <p>Joop Boonen rapporterede at update-policy-reglerne af typen 
    <q>subdomain</q>, blev håndhævet på forkert vis, hvilket gjorde det muligt 
    at opdatere alle dele af zonen, sammen med det tilsigtede 
    underdomæne.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 1:9.11.5.P4+dfsg-5.1+deb10u2.</p>

<p>Vi anbefaler at du opgraderer dine bind9-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende bind9, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4752.data"
