#use wml::debian::template title="Anpassningar"
#use wml::debian::translation-check translation="acad0e189af43d486b586d3628b2f3afa4b55523"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc

<toc-display/>

<toc-add-entry name="intro">Introduktion</toc-add-entry>

<p>Som de allra flesta av er vet är <a href="https://www.kernel.org/">Linux</a>
inte mer än en kärna, och under en lång tid kunde den bara köras på maskiner
med processorer i Intels x86-serie, från 386 och uppåt.</p>

<p>Detta är dock inte alls sant längre, Linuxkärnan har nu anpassats till ett
stort antal maskinvaruarkitekturer, och listan växer fortfarande. Vi försöker
hålla jämna steg och har anpassat Debiandistributionen för dessa system.
Vanligtvis består denna process av en besvärlig början (då vi försöker få
libc och den dynamiska länkaren att fungera ordentligt) för att sedan följas
av ett relativt rutinenligt, om än långt, arbete för att försöka kompilera om
alla våra paket under den nya maskinvaruarkitekturen. </p>

<p>Debian är ett operativsystem, inte en kärna (egentligen så är det mer än ett
operativsystem eftersom det innehåller tusentals tillämpningsprogram). Därför,
medan de flesta Debian-portar baseras på Linux, så finns det även anpassningar
baserade på FreeBSD, NetBSD och Hurd-kärnorna.</p>

<div class="important">
<p>Denna sida är under utveckling. Alla anpassningar har inte egna sidor ännu,
och de flesta av dem befinner sig på externa webbplatser. Vi arbetar på att
samla upplysningar om alla anpassningar, som kommer att speglas med
Debian-webbsidan.
Flera anpassningar kan <a href="https://wiki.debian.org/CategoryPorts">listas</a> på wikin.
</p>
</div>


<toc-add-entry name="portlist-released">Lista över officiella anpassningar</toc-add-entry>

<p>
Dessa anpassningar är anpassningarna med officiellt stöd från Debianprojektet,
och antingen en del av en utgåva eller tänkt att vara en del av en kommande
utgåva.
</p>

<table class="tabular" summary="">
<tbody>

<tr>
<th>Anpassning (port)</th>
<th>Arkitektur</th>
<th>Beskrivning</th>
<th>Tillagd</th>
<th>Status</th>
</tr>

<tr>
<td><a href="amd64/">amd64</a></td>
<td>64-bitars pc (amd64)</td>
<td>Anpassning till 64-bitars x86-processorer, med stöd för både 32-bitars och
64-bitars userland. Denna anpassning stöder AMD's 64-bitars
Opteron-, Athlon- och Sempron-processorer samt Intels Intel 64-arkitektur, bland
annat Pentium D samt olika Xeon och Core-serier.</td>
<td>4.0</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">utgiven</a></td>
</tr>

<tr>
<td><a href="arm/">arm64</a></td>
<td>64-bitars ARM (AArch64)</td>
<td>Anpassning till 64-bitars ARM-arkitekturen med den nya version 8 64-bitars
instruktionsuppsättningen (kallad AArch64), för processorer
som Applied Micro X-Gene, AMD Seattle och Cavium ThunderX.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">utgiven</a></td>
</tr>

<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>Anpassning till 32-bitars little-endian ARM-arkitekturen som använder
Embedded ABI, som stödjer ARM CPUer kompatibla med instruktionsuppsättningen
v5te. Denna anpassning drar inte fördel av flyttalsenheter (Floating Point
Units - FPU).</td>
<td>5.0</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">utgiven</a></td>
</tr>

<tr>
<td><a href="arm/">armhf</a></td>
<td>Hard Float ABI ARM</td>
<td>Anpassning till 32-bitars little-endian ARM-arkitekturen för kort och
enheter som levereras med en flyttalsenhet (FPU), och andra moderna ARM
CPU-funktioner. Denna anpassning kräver åtminstone en ARMv7 CPU med Thumb-2 och
VFPv3-O16 flyttalsstöd.</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">utgiven</a></td>
</tr>

<tr>
<td><a href="i386/">i386</a></td>
<td>32-bitars pc (i386)</td>
<td>Anpassning till 32-bitars x86-processorer, där Linux
ursprungligen utvecklades för Intels 386-processorer, därav förkortningen.
Debian stöder alla IA-32-processorer tillverkade av Intel (inklusive alla
Pentium och nyare Core Duo-maskiner i 32-bitarsläge), AMD (K6, alla Athlon,
Athlon64 i 32-bitarsläge), Cyrix och andra tillverkare. </td>
<td>1.1</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">utgiven</a></td>
</tr>

<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (64-bitars little-endian-läge)</td>
<td>
Anpassning till little-endian N64 ABI, MIPS64r1 ISA och
hårdvaruflyttal.
</td>
<td>9</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">utgiven</a></td>
</tr>

<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Anpassning för 64-bitars little-endian POWER-arkitekturen, som använder 
den nya Open Power ELFv2 ABIn.</td>
<td>8</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">utgiven</a></td>
</tr>

<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (64-bitars little endian)</td>
<td>Anpassning för 64-bit little-endian <a href="https://riscv.org/">RISC-V</a>,
en fri/öppen ISA.</td>
<td>13</td>
<td>testing</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>System z</td>
<td>Apassning för ett 64 bitars-userland till IBM System z-mainframes</td>
<td>7.0</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">utgiven</a></td>
</tr>


</tbody>
</table>

<toc-add-entry name="portlist-other">Lista över andra anpassningar</toc-add-entry>

<p>
Dessa anpassningar är antingen under utveckling som är menade att eventuellt
bli offiellt släppta arkitekturer, anpassningar som en gång i tiden var
arkitekturer med officiellt stöd men slutade släppas eftersom de missade
någon av utgåvekvalifikationerna eller hade begränsat utvecklarintresse, eller
anpasningar som inte längre fungerar och listas här av historiskt intresse.
</p>

<p>
Dessa anpassningar, när de fortfarande befinner sig under aktivt underhåll,
finns tillgängliga på infrastrukturen <url "https://www.ports.debian.org/">.
</p>

<div class="tip">
<p>
Det finns icke-officiella installationsavbildningar tillgängliga för
några av följande anpassningar i
<a href="https://cdimage.debian.org/cdimage/ports">https://cdimage.debian.org/cdimage/ports</a>.
Dessa avbildningar underhålls av motsvarande Debiananpassningsgrupp.
</p>
</div>


<table class="tabular" summary="">
<tbody>
<tr>
<th>Anpassning</th>
<th>Arkitektur</th>
<th>Beskrivning</th>
<th>Tillagd</th>
<th>Avslutat</th>
<th>Status</th>
<th>Ersatt av</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Anpassning till 64-bitarsarkitekturen RISC Alpha.</td>
<td>2.1</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>Anpassning till ARM-arkitekturen med gammalt ABI.
</td>
<td>2.2</td>
<td>6.0</td>
<td>död</td>
<td>armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">avr32</a></td>
<td>Atmel 32-bitars RISC</td>
<td>Anpassning till Atmel's 32-bitars RISC-arkitektur, AVR32. </td>
<td>-</td>
<td>-</td>
<td>död</td>
<td>-</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Anpassning till Hewlett-Packard's PA-RISC-arkitektur.
</td>
<td>3.0</td>
<td>6.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>32-bitars PC (i386)</td>
<td>
Anpassning till operativsystemet GNU Hurd, för 32-bitars x86-processorer.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="hurd/">hurd-amd64</a></td>
<td>64-bitars PC (amd64)</td>
<td>
Anpassning till operativsystemet GNU Hurd, för 64-bitars x86-processorer.
Den stödjer endast 64-bitar, inte 32-bitar vid sidan av 64-bitar-
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Anpassning till Intels första 64-bitars arkitektur. Obs: detta ska inte
förväxlas med de senaste 64-bitarstilläggen från Intel för Pentium 4 eller
Celeron-processorer, med namnet Intel 64; för dessa se amd64-anpassningen.
</td>
<td>3.0</td>
<td>8</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>64-bitars PC (amd64)</td>
<td>Anpassning till kärnan i FreeBSD tillsammans med glibc.
Den släpptes som första icke-Linux-anpassningen av Debian som en
teknologiförhandsvisning.
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">död</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>32-bitars PC (i386)</td>
<td>Anpassning till kärnan i FreeBSD tillsammans med glibc.
Den släpptes som första icke-Linux-anpassningen av Debian som en
teknologiförhandsvisning.
</td>
<td>6.0</td>
<td>8</td>
<td><a href="https://lists.debian.org/debian-devel/2023/05/msg00306.html">död</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>Anpassning till 32-bitars RISC-microprocessorer från  Renesas Technology.</td>
<td>-</td>
<td>-</td>
<td>död</td>
<td>-</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Anpassning till Motorola 64k-serien av processorer - speciellt
Sun3-sortimentet av arbetsstationer, persondatorer från Apple Macintosh och
hemmadatorerna Atari och Amiga.</td>
<td>2.0</td>
<td>4.0</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (big-endian mode)</td>
<td>Anpassning till MIPS-arkitekturen som används i (big-endian) SGI-maskiner.
<td>3.0</td>
<td>11</td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">död</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="mips/">mipsel</a></td>
<td>MIPS (little-endian mode)</td>
<td>Anpassning till MIPS-arkitekturen som används i (little-endian) Digital
DECstations.
</td>
<td>3.0</td>
<td>13</td>
<td><a href="https://lists.debian.org/debian-devel-announce/2023/09/msg00000.html">död</a></td>
<td>-</td>
</tr>
<tr>
<td><a href="netbsd/">netbsd-i386</a></td>
<td>32-bitars PC (i386)</td>
<td>
Anpassning till NetBSD-kärnan med libc, för 32-bitarsw x86-processorer.
</td>
<td>-</td>
<td>-</td>
<td>död</td>
<td>-</td>
</tr>
<tr>
<td><a href="netbsd/">netbsd-alpha</a></td>
<td>Alpha</td>
<td>
Anpassning till NetBSD-kärnan med libc, för 64-bitars Alpha-processorer.
</td>
<td>-</td>
<td>-</td>
<td>död</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Anpassning till öppen källkods-CPUn <a href="https://openrisc.io/">OpenRISC</a> 1200.</td>
<td>-</td>
<td>-</td>
<td>död</td>
<td>-</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>Anpassning för många Apple Macintosh PowerMac-modeller, och maskiner med
öppna arkitekturerna CHRP och PReP.</td>
<td>2.2</td>
<td>9</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>
Anpassning till "Signal Processing Engine"-hårdvaran som finns tillgänglig i
låg-energi 32-bitars FreeScale och IBM "e500"-CPUer.
</td>
<td>-</td>
<td>-</td>
<td>död</td>
<td>-</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 och zSeries</td>
<td>Anpassning till IBM S/390-servrar.</td>
<td>3.0</td>
<td>8</td>
<td>död</td>
<td>s390x</td>
</tr>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Anpassning för Sun UltraSPARC-serien av arbetsstationer, så väl som några
av dess efterförljare i sun4-arkitekturen.
</td>
<td>2.1</td>
<td>8</td>
<td>död</td>
<td>sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>64-bitars SPARC</td>
<td>
Anpassning till 64-bitars SPARC-processorer.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>
Anpassning till Hitachi SuperH-processorer. Stödjer även öppen källkods
<a href="https://j-core.org/">J-Core</a>-processorer.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>64-bitars PC med 32-bitars pekare</td>
<td>
Anpassning till amd64/x86_64 x32 ABI, som använder
amd64-instruktionsuppsättningen men med 32-bitars pekare, för att kombinera den
större registeruppsättningen från denna ISA med den mindre minnes- och
cache-avtrycket som resulterar från 32-bitars pekare.
</td>
<td>-</td>
<td>-</td>
<td>ports</td>
<td>-</td>
</tr>
</tbody>
</table>


<div class="note">
<p>Många av dator- och processornamnen som används ovan är varumärken och
registrerade varumärken hos sina tillverkare.</p>
</div>
